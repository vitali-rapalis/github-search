package com.vrapalis.www.githubsearch.assertions;

import com.vrapalis.www.githubsearch.exceptions.GithubSearchProcessException;
import lombok.experimental.UtilityClass;

import java.util.Date;
import java.util.Optional;

/**
 * GitHub Assertions.
 */
@UtilityClass
public class GithubSearchAssertion {

    public static void queryParameterAreCorrect(Optional<Date> date, Optional<String> language, Optional<Integer> size) throws GithubSearchProcessException {
        if (date.isPresent()) {
            try {
                var now = new Date();
                if (date.get().compareTo(now) > 0) {
                    throw new GithubSearchProcessException(new IllegalArgumentException("Illegal date, provided date is greater than date now"));
                }
            } catch (Exception ex) {
                throw new GithubSearchProcessException(ex);
            }
        }

        if (language.isPresent()) {
            if (!(language.get().length() >= 1)) {
                throw new GithubSearchProcessException(new IllegalArgumentException("Illegal date, provided language should contain characters, length >= 1"));
            }
        }

        if (size.isPresent()) {
            try {
                if (!(size.get() >= 1)) {
                    throw new IllegalArgumentException("Illegal date, provided size is not valid, size should be >=1");
                }
            } catch (Exception ex) {
                throw new GithubSearchProcessException(ex);
            }
        }
    }
}
