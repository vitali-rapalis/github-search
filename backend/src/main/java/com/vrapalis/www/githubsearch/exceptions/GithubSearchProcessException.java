package com.vrapalis.www.githubsearch.exceptions;

public class GithubSearchProcessException extends RuntimeException {

    public GithubSearchProcessException(Exception ex) {
        super(ex.getMessage());
    }
}
