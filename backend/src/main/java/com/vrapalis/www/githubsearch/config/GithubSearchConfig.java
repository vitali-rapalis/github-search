package com.vrapalis.www.githubsearch.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vrapalis.www.githubsearch.properties.GithubSearchProperties;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * GitHub main configuration.
 */
@Configuration
@EnableConfigurationProperties(GithubSearchProperties.class)
public class GithubSearchConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Open API 3.0 GithubSearch Specification.")
                        .description("This documentation provide you information about the rest api.")
                        .version("1.0.0")
                        .contact(new Contact()
                                .name("Vitali Rapalis")
                                .email("vitali.rapalis@gmail.com")
                                .url("https://konigsberger.com")));
    }
}
