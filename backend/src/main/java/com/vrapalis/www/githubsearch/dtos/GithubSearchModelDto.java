package com.vrapalis.www.githubsearch.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record GithubSearchModelDto(String name, @JsonProperty("full_name") String fullName, String description,
                                   String language, @JsonProperty("created_at") String createdAt,
                                   @JsonProperty("stargazers_count") Integer stargazersCount) {
}
