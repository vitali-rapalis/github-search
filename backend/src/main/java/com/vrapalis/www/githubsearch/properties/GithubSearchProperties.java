package com.vrapalis.www.githubsearch.properties;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * GitHub Properties.
 */
@Data
@Validated
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "github")
public class GithubSearchProperties {

    @NotBlank
    private String queryRegex;

    @NotBlank
    private String queryValueRegex;

    @NotBlank
    private String createdRegex;

    @NotBlank
    private String createdValueRegex;

    @NotBlank
    private String languageRegex;

    private String languageValueRegex;

    @NotBlank
    private String pageSizeRegex;

    @NotNull
    private Integer defaultPageSize = 10;

    @NotBlank
    private String url;
}
