package com.vrapalis.www.githubsearch.dtos;

public record GithubSearchExceptionResponseDto(Integer status, String reason, String tip) {
}
