package com.vrapalis.www.githubsearch.utility;

import lombok.experimental.UtilityClass;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * GithubSearch utility.
 */
@UtilityClass
public class GithubSearchUtility {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static String format(Date date) {
        return dateFormat.format(date);
    }
}
