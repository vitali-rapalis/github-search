package com.vrapalis.www.githubsearch.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public record GithubSearchDto(List<GithubSearchModelDto> items) {
}
