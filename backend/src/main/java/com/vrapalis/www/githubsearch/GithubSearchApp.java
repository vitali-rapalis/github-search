package com.vrapalis.www.githubsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * GitHub app.
 */
@EnableCaching
@SpringBootApplication
public class GithubSearchApp {

    /**
     * Starting point of the program.
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(GithubSearchApp.class, args);
    }
}
