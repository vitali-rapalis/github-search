package com.vrapalis.www.githubsearch.controller;

import com.vrapalis.www.githubsearch.dtos.GithubSearchDto;
import com.vrapalis.www.githubsearch.services.GithubSearchService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

/**
 * Rest controller.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class GithubSearchRestController {

    private final GithubSearchService githubSearchService;

    @GetMapping
    @Operation(tags = "Endpoints", summary = "Find github repositories", description = "Find repositories by parameters")
    @CachePut(value = "repositories", key = "{#date, #language, #size}")
    public ResponseEntity<GithubSearchDto> getRepositories(
            @Parameter(description = "The date parameter in ISO 8601 format (yyyy-MM-dd)", example = "2018-03-27", required = false)
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<Date> date,
            @Parameter(description = "The language parameter", example = "java", required = false)
            @RequestParam(required = false) Optional<String> language,
            @Parameter(description = "The size parameter", example = "10", required = false)
            @RequestParam(required = false) Optional<Integer> size) {
        return githubSearchService.getRepositories(date, language, size);
    }
}
