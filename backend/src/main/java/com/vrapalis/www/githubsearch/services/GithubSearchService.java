package com.vrapalis.www.githubsearch.services;

import com.vrapalis.www.githubsearch.dtos.GithubSearchDto;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Optional;

/**
 * GitHub search service.
 */
public interface GithubSearchService {

    /**
     * GitHub call with query parameters.
     *
     * @param date
     * @param language
     * @param size
     * @return
     */
    ResponseEntity<GithubSearchDto> getRepositories(Optional<Date> date, Optional<String> language, Optional<Integer> size);
}
