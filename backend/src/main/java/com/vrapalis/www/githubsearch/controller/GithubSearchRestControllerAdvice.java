package com.vrapalis.www.githubsearch.controller;

import com.vrapalis.www.githubsearch.dtos.GithubSearchExceptionResponseDto;
import com.vrapalis.www.githubsearch.exceptions.GithubSearchProcessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Controller exception handler.
 */
@RestControllerAdvice
public class GithubSearchRestControllerAdvice {

    @ExceptionHandler(GithubSearchProcessException.class)
    public ResponseEntity<GithubSearchExceptionResponseDto> badRequestException(GithubSearchProcessException ex) {
        var errorResponse = new GithubSearchExceptionResponseDto(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), "Navigate to api documentation.");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

}
