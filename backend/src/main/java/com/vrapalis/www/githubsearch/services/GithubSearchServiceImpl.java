package com.vrapalis.www.githubsearch.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vrapalis.www.githubsearch.assertions.GithubSearchAssertion;
import com.vrapalis.www.githubsearch.dtos.GithubSearchDto;
import com.vrapalis.www.githubsearch.exceptions.GithubSearchFetchDataException;
import com.vrapalis.www.githubsearch.exceptions.GithubSearchProcessException;
import com.vrapalis.www.githubsearch.properties.GithubSearchProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
public class GithubSearchServiceImpl implements GithubSearchService {

    private final RestTemplate restTemplate;

    private final ObjectMapper objectMapper;

    private final GithubSearchProperties props;

    @Override
    @Cacheable(cacheNames = "repositories", key = "{#date, #language, #size}")
    public ResponseEntity<GithubSearchDto> getRepositories(Optional<Date> date, Optional<String> language,
                                                           Optional<Integer> size) {
        try {
            GithubSearchAssertion.queryParameterAreCorrect(date, language, size);

            var url = "";
            var queryBuilder = new StringBuilder();

            if (date.isPresent()) {
                var dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                queryBuilder.append("+").append(props.getCreatedRegex().replace(props.getCreatedValueRegex(), dateFormat.format(date.get())));
            }

            if (language.isPresent()) {
                queryBuilder.append("+").append(props.getLanguageRegex().replace(props.getLanguageValueRegex(), language.get()));
            }

            if (size.isPresent()) {
                url = props.getUrl().replace(props.getPageSizeRegex(), Integer.toString(size.get()));
            } else {
                url = props.getUrl().replace(props.getPageSizeRegex(), Integer.toString(props.getDefaultPageSize()));
            }

            url = url.replace(props.getQueryRegex(), props.getQueryRegex().replace(props.getQueryValueRegex(), queryBuilder.toString()));

            log.info("Github rest api url: {}", url);
            return fetchDataFromGithub(url);
        } catch (Exception ex) {
            log.error(ex);
            throw new GithubSearchProcessException(ex);
        }
    }

    private ResponseEntity<GithubSearchDto> fetchDataFromGithub(String url) {
        try {
            final var headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            final var responseBody = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    new HttpEntity<>(headers),
                    String.class).getBody();

            var responseDto = new GithubSearchDto(List.of());
            responseDto = objectMapper.readValue(responseBody, GithubSearchDto.class);

            return ResponseEntity.ok(responseDto);
        } catch (Exception e) {
            log.error("Fetch repositories failed reason: {}", e.getMessage());
            throw new GithubSearchFetchDataException(e);
        }
    }
}
