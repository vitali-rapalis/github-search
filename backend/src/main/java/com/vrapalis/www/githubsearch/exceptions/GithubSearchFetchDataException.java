package com.vrapalis.www.githubsearch.exceptions;

public class GithubSearchFetchDataException extends RuntimeException {

    public GithubSearchFetchDataException(Throwable ex) {
        super("Fetch data from github failed, reason: " + ex.getLocalizedMessage());
    }
}
