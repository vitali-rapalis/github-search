package com.vrapalis.www.githubsearch.assertions;


import com.vrapalis.www.githubsearch.exceptions.GithubSearchProcessException;
import com.vrapalis.www.githubsearch.utility.GithubSearchUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Optional;

@DisplayName("Github assertions test.")
class GithubSearchAssertionTest {

    private String correctDateString = "2018-01-01";

    @Test
    @DisplayName("Throw exception for illegal date test")
    void forIllegalDateExceptionShouldBeThrownTest() throws ParseException {
        // Give
        var incorectDate = "2024-01-01";
        var givenDate = GithubSearchUtility.dateFormat.parse(incorectDate);

        // When/Then
        Assertions.assertThatThrownBy(() -> GithubSearchAssertion.queryParameterAreCorrect
                        (Optional.of(givenDate), Optional.empty(), Optional.empty()))
                .isInstanceOf(GithubSearchProcessException.class)
                .hasMessageContaining("date");
    }

    @Test
    @DisplayName("Throw exception for illegal language test")
    void forIllegalLanguageExceptionShouldBeThrownTest() throws ParseException {
        // Given
        var incorrectLanguage = "";
        var correctDate = GithubSearchUtility.dateFormat.parse(correctDateString);

        // When/Then
        Assertions.assertThatThrownBy(() -> GithubSearchAssertion.queryParameterAreCorrect
                        (Optional.of(correctDate), Optional.of(incorrectLanguage), Optional.empty()))
                .isInstanceOf(GithubSearchProcessException.class).hasMessageContaining("language");
    }

    @Test
    @DisplayName("Throw exception for illegal size test")
    void forIllegalSizeThrowExceptionTest() throws ParseException {
        // Given
        var language = "Java";
        var givenDate = GithubSearchUtility.dateFormat.parse(correctDateString);

        // When/Then
        Assertions.assertThatThrownBy(() -> GithubSearchAssertion.queryParameterAreCorrect
                        (Optional.of(givenDate), Optional.of(language), Optional.of(0)))
                .isInstanceOf(GithubSearchProcessException.class).hasMessageContaining("size");

    }
}