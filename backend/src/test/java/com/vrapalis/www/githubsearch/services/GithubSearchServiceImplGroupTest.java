package com.vrapalis.www.githubsearch.services;


import com.vrapalis.www.githubsearch.config.GithubSearchConfig;
import com.vrapalis.www.githubsearch.dtos.GithubSearchModelDto;
import com.vrapalis.www.githubsearch.exceptions.GithubSearchProcessException;
import com.vrapalis.www.githubsearch.utility.GithubSearchUtility;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackageClasses = GithubSearchService.class)
@TestPropertySource("/application.properties")
@Import(GithubSearchConfig.class)
@DisplayName("Github service group test")
public class GithubSearchServiceImplGroupTest {

    @Autowired
    private GithubSearchService githubSearchService;

    @Test
    @DisplayName("Should throw exception for illegal parameters test")
    void shouldThrowExceptionForIllegalParameterTest() throws ParseException {
        // Given
        var illegalDate = Optional.of(GithubSearchUtility.dateFormat.parse("2060-01-01"));

        // When/Then
        Assertions.assertThatThrownBy(() -> githubSearchService
                        .getRepositories(illegalDate, Optional.empty(), Optional.empty()))
                .isInstanceOf(GithubSearchProcessException.class)
                .hasMessageContaining("Illegal date");
    }

    @Test
    @DisplayName("Should return repositories without parameters sorted by stars test")
    void shouldReturnWithoutParameterRepositoriesSortedByStarsTest() {
        // When
        var repositories = githubSearchService.getRepositories(Optional.empty(), Optional.empty(), Optional.empty())
                .getBody();

        var firstItem = repositories.items().get(0);
        var secondItem = repositories.items().get(1);

        // Then
        Assertions.assertThat(firstItem.stargazersCount()).isGreaterThan(secondItem.stargazersCount());
    }

    @Test
    @DisplayName("Provided date should return the repositories according to date test")
    void shouldReturnAccordingToDateTest() throws ParseException {
        // Given
        var givenDate = GithubSearchUtility.dateFormat.parse("2020-01-01");

        // When
        var repositories = githubSearchService.getRepositories(Optional.of(givenDate), Optional.empty(), Optional.empty())
                .getBody();

        // Then
        var testDate = GithubSearchUtility.dateFormat.parse("2018-01-01");
        for (GithubSearchModelDto item : repositories.items()) {
            Assertions.assertThat(GithubSearchUtility.dateFormat.parse(item.createdAt())).isAfter(testDate);
        }
    }

    @Test
    @DisplayName("Should return items according to language test")
    void shouldReturnItemsAccordingToLanguageTest() {
        // Given
        var givenLanguage = "javaScript";

        // When
        var repositories = githubSearchService.getRepositories(Optional.empty(), Optional.of(givenLanguage), Optional.empty())
                .getBody();

        // Then
        for (GithubSearchModelDto item : repositories.items()) {
            Assertions.assertThat(item.language()).isEqualToIgnoringCase(givenLanguage);
        }
    }

    @Test
    @DisplayName("Should return items according size test")
    void shouldReturnItemsAccordingSizeTest() {
        // Given
        var givenSize = 20;

        // When
        var repositories = githubSearchService.getRepositories(Optional.empty(), Optional.empty(), Optional.of(givenSize))
                .getBody();

        // Then
        Assertions.assertThat(repositories.items().size()).isEqualTo(givenSize);
    }
}
