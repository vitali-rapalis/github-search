package com.vrapalis.www.githubsearch.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vrapalis.www.githubsearch.TestUtility;
import com.vrapalis.www.githubsearch.config.GithubSearchConfig;
import com.vrapalis.www.githubsearch.dtos.GithubSearchDto;
import com.vrapalis.www.githubsearch.dtos.GithubSearchExceptionResponseDto;
import com.vrapalis.www.githubsearch.exceptions.GithubSearchProcessException;
import com.vrapalis.www.githubsearch.services.GithubSearchService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;


import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@Import(GithubSearchConfig.class)
@WebMvcTest(GithubSearchRestController.class)
@DisplayName("Github rest controller component group test")
class GithubSearchRestControllerGroupTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GithubSearchService githubSearchService;

    private ResponseEntity<GithubSearchDto> givenResponseMock;

    @BeforeEach
    void setUp() {
        givenResponseMock = ResponseEntity.ok(TestUtility.givenDtoMock());
    }

    @Nested
    @DisplayName("Test rest controller without query parameter group test")
    class GithubSearchRestControllerWithoutQueryParameterGroupTest {

        @Test
        @DisplayName("No query parameter passed to controller should return 200 http status test")
        void noQueryParameterPassedToRestControllerShouldReturnOkHttpStatus() throws Exception {
            // When
            Mockito.when(githubSearchService.getRepositories(Optional.empty(), Optional.empty(), Optional.empty()))
                    .thenReturn(givenResponseMock);
            MockHttpServletResponse response = callRestController("");
            GithubSearchDto githubSearchDto = objectMapper.readValue(response.getContentAsString(), GithubSearchDto.class);

            // Then
            Assertions.assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
            Assertions.assertThat(githubSearchDto).isEqualTo(givenResponseMock.getBody());

            // Verify
            Mockito.verify(githubSearchService, Mockito.times(1))
                    .getRepositories(Optional.empty(), Optional.empty(), Optional.empty());
        }
    }

    @Nested
    @DisplayName("Test rest controller with illegal query parameter group test")
    class GithubSearchRestControllerWithIllegalParameterGroupTest {

        @Test
        @DisplayName("For illegal date controller should return bad request test")
        void illegalDateControllerShouldReturnBadRequestTest() throws Exception {
            // When
            Mockito.when(githubSearchService.getRepositories(Mockito.any(Optional.class), Mockito.any(Optional.class), Mockito.any(Optional.class)))
                    .thenThrow(new GithubSearchProcessException(new IllegalArgumentException("Illegal date")));
            MockHttpServletResponse response = callRestController("?date=2060-01-01");
            var exceptionResponseDto = objectMapper.readValue(response.getContentAsString(), GithubSearchExceptionResponseDto.class);

            // Then
            Assertions.assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
            Assertions.assertThat(exceptionResponseDto.reason()).contains("Illegal date");
        }
    }

    @Nested
    @DisplayName("Test rest controller with legal query parameter group test")
    class GithubSearchRestControllerWithLegalParameterGroupTest {
        @Test
        @DisplayName("Rest controller for legal parameter should return 200 status test")
        void callRestControllerWithLegalParameterShouldReturnHttpStatus0k() throws Exception {
             // When
            Mockito.when(githubSearchService.getRepositories(Mockito.any(Optional.class), Mockito.any(Optional.class), Mockito.any(Optional.class)))
                    .thenReturn(givenResponseMock);
            MockHttpServletResponse response = callRestController("?date=2014-01-01&language=java&size=20");
            GithubSearchDto githubSearchDto = objectMapper.readValue(response.getContentAsString(), GithubSearchDto.class);

           // Then
            Assertions.assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
            Assertions.assertThat(githubSearchDto).isEqualTo(givenResponseMock.getBody());
        }
    }

    private MockHttpServletResponse callRestController(String queries) throws Exception {
        return mvc.perform(get("/api" + queries).accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

}
