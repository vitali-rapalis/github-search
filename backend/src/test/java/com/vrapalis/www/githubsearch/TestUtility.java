package com.vrapalis.www.githubsearch;

import com.vrapalis.www.githubsearch.dtos.GithubSearchDto;
import com.vrapalis.www.githubsearch.dtos.GithubSearchModelDto;
import lombok.experimental.UtilityClass;

import java.util.Date;
import java.util.List;

@UtilityClass
public class TestUtility {
    public static GithubSearchModelDto givenModelDtoMock() {
        return GithubSearchModelDto.builder()
                .name("Mockito")
                .fullName("Mockito Core")
                .createdAt(new Date().toString())
                .description("Testing library")
                .language("Java")
                .stargazersCount(3670)
                .build();
    }

    public static GithubSearchDto givenDtoMock() {
        return new GithubSearchDto(List.of(givenModelDtoMock()));
    }

}
