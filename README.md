# GITHUB SEARCH APPLICATION

---

> The app that searches GitHub repositories.

## How to run

- Clone project (terminal command)
```
git clone git@gitlab.com:vitali-rapalis/github-search.git
```

- Gradle run (terminal command)
```
./gradlew bootRun
```

- Open in the browser [api swagger ui](http://localhost:8080/swagger-ui/index.html) to test the endpoints
```
http://localhost:8080/swagger-ui/index.html
```

- Open in the browser [jacoco test report](http://localhost:8080/jacoco/index.html) to verify coverage report
```
http://localhost:8080/jacoco/index.html
```

- **Easy Way** use docker, the image pushed to docker hub
```
docker run --rm -p 8080:8080 vrapalis/github-search
```
